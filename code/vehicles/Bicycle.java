package vehicles;

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer(){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    }

    public Bicycle(String userManufacturer, int userNumberGears, double userMaxSpeed){
        this.manufacturer = userManufacturer;
        this.numberGears = userNumberGears;
        this.maxSpeed = userMaxSpeed;
    }

    public String toString(){
        String toReturn = "Manufacturer: " + this.manufacturer + ", Number of gears: " + this.numberGears + ", Max speed: " + this.maxSpeed;
        return toReturn;
    }
}
