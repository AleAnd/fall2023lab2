package application;
import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[]args){
        Bicycle bike1 = new Bicycle("Canyon", 1, 30.5);
        Bicycle bike2 = new Bicycle("Trek", 7, 35.2);
        Bicycle bike3 = new Bicycle("Suzuki", 6, 39);
        Bicycle bike4 = new Bicycle("Diamondback", 10, 42.4);
    
        Bicycle[] arrayOfBikes = {bike1,bike2,bike3,bike4} ;

        for(int i = 0; i < arrayOfBikes.length; i++){
            System.out.println(arrayOfBikes[i]);
        }
    
    
    }
}
